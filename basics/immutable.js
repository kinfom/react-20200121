// primitive data
var a = 9; // immutable
var b = a; // immutable
console.log(a, b);
b = 6;
console.log(a, b);

// reference variables
var x = [1, 2, 3, 4]; // mutated - changed
// var y = x; // assigning only the reference
var y = [...x, 6]; // spread operator
console.log(x, y);
y.push(5);
console.log(x, y);

var p = { name: "john", location: "US" };
var q = { ...p, email: "john@example.com" };
console.log(p, q);
q.gender = "male";
console.log(p, q);

// rest operator

function sum(...x) {
  // rest operator
  console.log(x);
  let sum = 0;
  x.forEach(ele => (sum = sum + ele));
  //   return a + b + c;
  console.log(sum);
}
console.log(sum(4, 5, 6, 89));
