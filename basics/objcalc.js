let calculator = {
  a: 2,
  b: 4,
  add: function() {
    let add = () => {
      console.log(this.a + this.b);
    };
    add();
  },
  sub: function() {
    console.log(this.a - this.b);
  },
  mul: function() {
    console.log(this.a * this.b);
  }
};
calculator.a = 7;
calculator.add();
calculator.sub();
calculator.mul();
