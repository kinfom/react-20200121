// es5
// function printName(name) {
//   let myname = name;
//   console.log(myname);
// }

//es5
// var printName = function() {
//   console.log("doe");
// };

//es6 - fat arrow function
let printName = (name, age) => {
  let incr = 2;
  age = age + incr; // increments
  //  console.log(name + " will be " + age + " years old after " + incr + " years"); // printing message
  console.log(`${name} will be ${age} years old after ${incr} years`); // template literals
};
printName("krishna", 32);
