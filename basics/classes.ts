class Numbers {
  a = 1; // data members
  even() {
    // member function
    return this.a % 2 == 0 ? "even" : "odd"; // ternary operation
  }
}
let num = new Numbers(); // create an instance (object)
num.a = 6;
console.log(num.even());
