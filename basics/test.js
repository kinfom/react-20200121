// var let const
// var - function scope
// let - block scope
// window object
// const name = "john"; // constant - never changes - block level
var a = 7;
function varLetDemo() {
  //   name = "doe";
  var a = 12; // function scope
  if (a > 10) {
    const name = "doe";
    let a = 5; // block scope
    var b = 9;
    console.log(a + b);
    a++;
    console.log("a from block", a);
    console.log("name from block", name);
  }
  // name = "derek";
  console.log(name);
  console.log("a from function", a); // 6
}
varLetDemo();
// console.log("name from window", name);
console.log("a from window scope", a);
