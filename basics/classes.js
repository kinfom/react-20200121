var Numbers = (function () {
    function Numbers() {
        this.a = 1; // data members
    }
    Numbers.prototype.even = function () {
        // member function
        return this.a % 2 == 0 ? "even" : "odd"; // ternary operation
    };
    return Numbers;
})();
var num = new Numbers(); // create an instance (object)
num.a = 6;
console.log(num.even());
