import React, { Component } from "react";
import E from "./E";
class C extends Component {
  state = {};
  render() {
    return (
      <div style={{ backgroundColor: "blue" }}>
        <h2>C</h2>
        <E />
      </div>
    );
  }
}

export default C;
