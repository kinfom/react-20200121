import React, { Component } from "react";
import { UserConsumer } from "./UserContext";
class D extends Component {
  state = { uname: "" };
  render() {
    return (
      <div>
        <h2>D</h2>
        {this.state.uname}
        <UserConsumer>
          {username => {
            // this.setState({ uname: username });
            return <div>Hellow {username}</div>;
          }}
        </UserConsumer>
      </div>
    );
  }
}

export default D;
