import React, { Component } from "react";
import D from "./D";
class B extends Component {
  state = {};
  render() {
    return (
      <div style={{ backgroundColor: "green" }}>
        <h2>B</h2>
        <D />
      </div>
    );
  }
}

export default B;
