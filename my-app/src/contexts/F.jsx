import React, { Component } from "react";
import { UserConsumer } from "./UserContext";
class F extends Component {
  state = {};
  render() {
    return (
      <div>
        <h2>F</h2>
        <UserConsumer>
          {username => {
            return <div>Hellow {username}</div>;
          }}
        </UserConsumer>
      </div>
    );
  }
}

export default F;
