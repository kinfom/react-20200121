import React, { Component } from "react";
import B from "./B";
class A extends Component {
  constructor(props) {
    super(props);
    this.state = { state: "" };
    console.log("Component A constructor lifecycle");
    this.inputref = React.createRef();
  }
  // static getDerivedStateFromProps(props, state) {
  //   console.log("Component A getDerivedState lifecycle", props, state);
  //   return null;
  // }
  componentDidMount() {
    console.log("Component A componentDidMount lifecycle");
  }
  render() {
    console.log("Component A render lifecycle");
    return (
      <div>
        <h1>Component A</h1>
        {/* <button
          onClick={() => {
            this.setState({ state: "state updated" });
          }}
        >
          Update state
        </button> */}
        {/* Current State: {this.state.state}
        <B data={this.state.state} /> */}
        <input type="text" ref = {this.inputref}/>
        <button onClick={()=>console.log(this.inputref.current.value)}>submit</button>
      </div>
    );
  }
  componentDidUpdate() {
    console.log("Component A componentDidUpdate lifecycle");
  }
}
export default A;
