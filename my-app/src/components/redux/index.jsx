const { createStore } = require("redux");
const initialState = { age: 21 };

const reducer = (state = initialState, action) => {
  // 2
  switch (action.type) {
    case "DISPLAY": // 5
      console.log("state", state, "action", action);
      return state;
      break;
    case "ADD": //7
      const newState = { ...state };
      newState.age += action.payload;
      return newState;
      break;

    default:
      break;
  }
};

const store = createStore(reducer); // 1

store.subscribe(() => {
  console.log("state changed" + JSON.stringify(store.getState()));
}); // 3

store.dispatch({ type: "DISPLAY" }); // 4
store.dispatch({ type: "ADD", payload: 3 }); // 6
