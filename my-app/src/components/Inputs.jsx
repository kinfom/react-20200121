import React, { Component } from 'react';
class Input extends Component {
  state = { number:'', error: false, message:'', fname:'', lname:'', submitted:false }
  render() { 
    return ( <div>
      <form onSubmit={this.submitHandler}>
      <p style={{color: this.state.error?'red':'green'}}>

{this.state.message}
</p>

      <label htmlFor="">Firstname</label>
        <input type="text" name="firstname" onChange={this.fieldHandler}/>
        <br/>
        <label htmlFor="">LastName</label>
        <input type="text" name="lastname" onChange={this.fieldHandler}/>
        <br/>
        <input type="submit"/>
        
      </form>
      <input type="text" value={this.state.number} onChange={this.numberHandler}/>
      <br/>
    </div> );
  }
  fieldHandler = (e) => {
    switch(e.target.name ) {
      case 'firstname': this.setState({ fname: e.target.value  }); break;
      case 'lastname': this.setState({ lname: e.target.value  }); break;
    }
  }
  numberHandler = (event) => {
    console.log(event.target.value)
    if (event.target.value%2===0) {
      this.setState({ error:false, message: 'The number is even'  });
    } else {
      this.setState({ error:true, message: 'The number is odd'  });
    }
    this.setState({ number: event.target.value });
  }
  submitHandler = (e) => {
    e.preventDefault();
    console.log('form submitted');
    if (!this.state.fname) {
      this.setState({ error: true, message: 'Please enter firstname' });
    } else if (!this.state.lname) {
      this.setState({ error: true, message: 'Please enter lastname' });
    }
    else {
      let formData = {fname: this.state.fname, lname: this.state.lname}
      this.setState({ error: false, message: 'Thank you for submitting the form' });
      console.log(formData);
    }

  }
}
export default Input;