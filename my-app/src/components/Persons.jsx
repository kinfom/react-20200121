import React, { Component } from "react";
import { Fragment } from "react";
import Person from "./Person";
import { persons } from "./../data/persons";
class Persons extends Component {
  state = {
    personsList: persons,
    title: ""
  };

  render() {
    // console.log(persons);
    // let email = "john@something.com";
    return (
      <Fragment>
        <h1>{this.state.title}</h1>
        {this.state.personsList.map(user => (
          //   <h2>{person.name}</h2>
          <Person key={user.name} data={user} handleTitle={this.titleHandler} />
        ))}
        {/* <Person data={this.state} email={email} /> */}
      </Fragment>
    );
  }
  titleHandler = name => {
    this.setState({
      //   title: this.state.title.length > 0 ? "" : "Displaying Users"
      title: `${name} set the title`
    });
  };
}

export default Persons;
