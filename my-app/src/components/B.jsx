import React, { Component } from "react";
class B extends Component {
  constructor(props) {
    super(props);
    this.state = { state: this.props.data };
    console.log("Component B constructor lifecycle");
  }
  static getDerivedStateFromProps(props, state) {
    console.log("Component B getDerivedState lifecycle", props, state);
    return null;
  }
  componentDidMount() {
    console.log("Component B componentDidMount lifecycle");
  }
  render() {
    console.log("Component B render lifecycle");
    return (
      <div>
        <h1>Component B</h1>
        Current data prop: {this.props.data}
      </div>
    );
  }
  componentDidUpdate() {
    console.log("Component B componentDidUpdate lifecycle");
  }
}
export default B;
