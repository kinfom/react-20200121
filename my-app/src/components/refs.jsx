import React, { Component } from 'react';
class Refs extends Component {
    constructor(props) {
        super(props);
        this.state = {fname: '', lname: '', error: false, message:''};
        this.fnameref = React.createRef();
    }
    render() { 
        return ( <div>
            <input type="text" ref={this.fnameref} name="firstname"/>
            <button onClick={()=>{console.log(this.fnameref.current.name)}}> submit</button>
        </div> );
    }
}
 
export default Refs;