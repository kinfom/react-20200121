import React from "react";
import Third from "./Third";
// import appstyle from "./third.style.css";
// const myclass = { fontColor: "red" };
const First = () => {
  return (
    <div>
      <h1>This is first component</h1>
      <Third />
    </div>
  );
};

export default First;
