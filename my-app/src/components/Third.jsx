import React, { Component, Fragment } from "react";
// import "./third.style.css";
import mystyles from "./third.module.css";
class Third extends Component {
  state = {};

  render() {
    const style = { color: "red", fontSize: "44px", backgroundColor: "green" };
    return (
      <Fragment>
        {/* style with className */}
        <h1 className={mystyles.redHeading}>This is third component</h1>
        {/* inline styling */}
        <p id="myp" style={style}>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Recusandae,
          accusamus soluta. Facilis, exercitationem! Veniam libero et dolorum
          error numquam eaque. Nisi, odio eius? Aut impedit tempora, minima
          consequatur nisi fuga?
        </p>
        <p className={mystyles.bluePara}>lorem ipsum dolor</p>
      </Fragment>
    );
  }
}

export default Third;
