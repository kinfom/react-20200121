import React, { useState } from "react";
function Stateinfunction() {
  const [number, setNumber] = useState(1);
  const [name, setName] = useState("john");
  const [address, setAddress] = useState({
    door: 45,
    street: "down street",
    city: "new york",
    country: ""
  });
  return (
    <div>
      <h2>Number is: {number}</h2>
      <h2>Name is: {name}</h2>
      <h2>
        Address: {address.door}, {address.street}, {address.city},{" "}
        {address.country}
      </h2>
      <input
        type="text"
        placeholder="door no"
        onChange={e => {
          setAddress({ ...address, door: e.target.value, country: "US" });
        }}
      />
      <button onClick={() => setNumber(number + 1)}>Increment</button>
    </div>
  );
}
export default Stateinfunction;
