import React, { Component } from "react";
class Stateinclass extends Component {
  state = { number: 1 };
  clickHandler = () => {
    console.log(this.state);
    this.setState({ number: this.state.number + 1 });
  };
  render() {
    return (
      <div>
        <h2>Number is: {this.state.number}</h2>
        <button onClick={this.clickHandler}>Increment</button>
      </div>
    );
  }
}

export default Stateinclass;
