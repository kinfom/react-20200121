import React, { useState, useEffect } from "react";
function UseEffectDemo() {
  const [number, setNumber] = useState(0);
  const [name, setName] = useState("alex");
  useEffect(() => {
    console.log("use effect called");
  }, [name]);
  return (
    <div>
      <h2>{number}</h2>
      <input type="text" value={name} onChange={e => setName(e.target.value)} />
      <button onClick={() => setNumber(number + 1)}>Click</button>
    </div>
  );
}
export default UseEffectDemo;
