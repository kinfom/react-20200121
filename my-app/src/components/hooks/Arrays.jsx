import React, { useState, useRef } from "react";
function Arrays() {
  const [names, setNames] = useState([]);
  const [name, setName] = useState("john");
  const inputref = useRef();
  const divref = useRef();
  const addItem = () => {
    console.log(inputref.current.value);
    setNames([...names, inputref.current.value]);
    divref.current.style.background = inputref.current.value;
  };
  return (
    <div>
      <input type="text" ref={inputref} />
      <button onClick={addItem}>Add</button>
      <div
        ref={divref}
        style={{ height: "100px", width: "100px", background: "red" }}
      ></div>
      <h2>Names</h2>
      <ul>
        {names.map(name => (
          <li>{name}</li>
        ))}
      </ul>
    </div>
  );
}
export default Arrays;
