import React, { useRef } from "react";
function EvenOdd() {
  const inputref = useRef();
  const msgref = useRef();
  const checkHandler = () => {
    if (inputref.current.value % 2 == 0) {
      msgref.current.innerText = "The number is even";
      msgref.current.style.color = "green";
    } else {
      msgref.current.innerText = "The number is odd";
      msgref.current.style.color = "red";
    }
  };
  return (
    <div>
      <input type="text" ref={inputref} />
      <button onClick={checkHandler}>check</button>
      <p ref={msgref}></p>
    </div>
  );
}
export default EvenOdd;
