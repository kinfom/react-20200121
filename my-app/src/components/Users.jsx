import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
class Users extends Component {
  constructor(props) {
    super(props);
    this.titleref = React.createRef();
    this.bodyref = React.createRef();
    this.useridref = React.createRef();
  }
  state = { data: [], id: "", formdata: { title: "", body: "", userId: "" } };
  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/users/")
      .then(res => {
        console.log(res.data);
        this.setState({ data: res.data });
      })
      .catch(err => console.log("error", err));
  }
  getData = e => {
    console.log(e.target.value);
    this.setState({ id: e.target.value });
    // axios
    //   .get("https://jsonplaceholder.typicode.com/users/" + e.target.value)
    //   .then(res => {
    //     console.log(res.data);
    //     this.setState({ data: [] });
    //     this.setState({
    //       data: [...this.state.data, res.data]
    //     });
    //   });
    const users = [...this.state.data];
  };
  submitHandler = e => {
    console.log(this.titleref);
    e.preventDefault();

    const formdata = {
      title: this.titleref.current.value,
      body: this.bodyref.current.value,
      userId: this.useridref.current.value
    };

    console.log(formdata);
    axios
      .post("https://jsonplaceholder.typicode.com/posts", formdata)
      .then(res => {
        console.log(res);
      })
      .catch(err => console.log(err));
  };
  render() {
    console.log(this.state.formdata);
    const users = [...this.state.data];
    return (
      <div>
        {/* <input type="text" value={this.state.id} onChange={this.getData} /> */}
        {/* <form onSubmit={this.submitHandler}>
          <input type="text" ref={this.titleref} />
          <br />
          <input type="text" ref={this.bodyref} />
          <br />
          <input type="text" ref={this.useridref} />
          <br />
          <input type="submit" />
        </form> */}
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Website</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {users.map(user => (
              <tr key={user.id}>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.phone}</td>
                <td>{user.website}</td>
                <td>
                  <Link to={"User/" + user.id}>View</Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Users;
