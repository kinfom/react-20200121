import React, { Component } from "react";
class Second extends Component {
  //    state = {  }
  render() {
    return (
      <div>
        <h1>This is second component</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque beatae
          necessitatibus nisi cumque ut possimus neque omnis repudiandae iusto
          delectus! Dicta placeat maiores cum vero nam eos odio repellat
          tempore!
        </p>
      </div>
    );
  }
}

export default Second;
