import React, { Component } from "react";
import { Fragment } from "react";

class Person extends Component {
  state = {
    greeting: "",
    date: new Date()
  };
       greetingHandler = () => {
    this.setState({ greeting: `Welcome ${this.props.data.name}` });
    // this.state.greeting = `Welcome ${this.props.data.name}`;
  };
  render() {
    console.log(this.props);
    const personStyle = {
      backgroundColor: "pink",
      padding: "10px",
      border: "1px solid green",
      marginBottom: "10px"
    };
    return (
      <Fragment>
        <div style={personStyle}>
          <h4>{this.state.greeting}</h4>
          <p>{this.state.date.getFullYear()}</p>
          <h5>Person: {this.props.data.name}</h5>
          <h5>Age: {this.props.data.age}</h5>
          <h5>Email: {this.props.data.email}</h5>
          <button onClick={this.greetingHandler}>Greet User</button>
          <button onClick={() => this.props.handleTitle(this.props.data.name)}>
            Toggle Page Title
          </button>
        </div>
      </Fragment>
    );
  }
}

export default Person;
