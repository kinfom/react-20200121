export const persons = [
  { name: "john", age: 32, email: "john@example.com" },
  { name: "doe", age: 22, email: "doe@example.com" },
  { name: "alex", age: 42, email: "alex@example.com" }
];
