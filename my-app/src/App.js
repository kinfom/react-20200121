import React, { Component } from "react";
// import "./App.css";
// import appstyles from "./components/third.style.css";
import Main from "./routing/Main";
import First from "./components/First";
import Second from "./components/Second";
import Persons from "./components/Persons";
import Inputs from "./components/Inputs";
// import A from "./components/LifeCycle";
import Refs from "./components/refs";
import A from "./contexts/A";
import B from "./contexts/B";
import C from "./contexts/C";
import { UserProvider } from "./contexts/UserContext";
import Users from "./components/Users";
import Stateinclass from "./components/hooks/Stateinclass";
import Stateinfunction from "./components/hooks/Stateinfunction";
import Arrays from "./components/hooks/Arrays";
import EvenOdd from "./components/hooks/EvenOdd";
import UseEffectDemo from "./components/hooks/UseEffectDemo";
import redux from "./components/redux";
class App extends Component {
  state = { username: "john" };
  inputHandler = e => {
    this.setState({ username: e.target.value });
  };
  render() {
    return (
      <redux />
      // <UseEffectDemo />
      // <EvenOdd />
      // <Arrays />
      // <Stateinfunction />
      // <Stateinclass />
      // <Main />
      // <Users />
      // <div style={{ backgroundColor: "#eee" }}>
      //   <input
      //     type="text"
      //     value={this.state.username}
      //     onChange={this.inputHandler}
      //   ></input>
      //   <UserProvider value={this.state.username}>
      //     <A />
      //     <B />
      //     <C />
      //   </UserProvider>
      // </div>
      // <Refs />
      // <A />
      // <Inputs />
      // <Persons />
      // JSX
      // <div className="mydiv">
      //   <h1>Hellow World</h1>

      //   <First />
      //   <Second />
      // </div>
      // return React.createElement(
      //   "null",
      //   { id: "mydiv", className: "myclass2" },
      //   React.createElement("h1", null, "hello world")
      // );
    );
  }
}
export default App;
