import React, { Component } from "react";
import Users from "./../components/Users";
import {
  BrowserRouter as Router,
  Link,
  NavLink,
  Switch,
  Route
} from "react-router-dom";
import About from "./About";
import Contact from "./Contact";
import Company from "./Company";
import Userinfo from "./Userinfo";
class Main extends Component {
  state = {};
  render() {
    return (
      <Router>
        <div>
          <ul>
            <li>
              <NavLink
                to="/"
                exact
                activeStyle={{ color: "green", fontWeight: "bold" }}
              >
                Home
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/about"
                activeStyle={{ color: "green", fontWeight: "bold" }}
              >
                About
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/contact"
                activeStyle={{ color: "green", fontWeight: "bold" }}
              >
                Contact
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/about-company"
                activeStyle={{ color: "green", fontWeight: "bold" }}
              >
                Company
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/users"
                activeStyle={{ color: "green", fontWeight: "bold" }}
              >
                Users
              </NavLink>
            </li>
            {/* <li>
              <Link to="/Users/1">View</Link>
            </li> */}
          </ul>
          <hr />
          <Switch>
            <Route exact path="/" render={() => <h1>Home Page</h1>} />
            <Route path="/about" component={About}></Route>
            <Route path="/about-company" component={Company}></Route>
            <Route path="/contact">
              <Contact />
            </Route>
            <Route exact path="/users">
              <Users />
            </Route>
            <Route path="/User/:id" component={Userinfo}></Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default Main;
