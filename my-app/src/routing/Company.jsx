import React, { Component } from "react";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import About from "./About";
class Company extends Component {
  state = {};
  render() {
    return (
      <Router>
        <div>
          <h1>Company Page</h1>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugiat
            neque doloremque nemo error blanditiis. Est autem quas voluptates
            totam veritatis, quasi ipsam similique error architecto assumenda a
            aspernatur? Aspernatur, quos.{" "}
          </p>
          <Link to="/company/about">
            <button>Read More</button>
          </Link>
          <Switch>
            <Route path="/company/about">
              <About />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default Company;
