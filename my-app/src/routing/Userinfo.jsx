import React, { Component } from "react";
import axios from "axios";
class Userinfo extends Component {
  state = {
    id: this.props.match.params.id,
    data: {
      name: "",
      email: "",
      username: "",
      phone: "",
      address: { city: "" }
    }
  };
  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/users/" + this.state.id)
      .then(res => {
        console.log(res);
        this.setState({ data: res.data });
      });
  }
  render() {
    console.log(this.props.match.params.id);
    return (
      <div>
        <h2>User Information with user id {this.state.id}</h2>
        <ul>
          <li>Name: {this.state.data.name}</li>
          <li>Username: {this.state.data.username}</li>
          <li>Email: {this.state.data.email}</li>
          <li>Phone: {this.state.data.phone}</li>
          <li>City: {this.state.data.address.city}</li>
        </ul>
      </div>
    );
  }
}

export default Userinfo;
