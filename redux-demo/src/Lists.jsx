import React, { useEffect } from "react";
import { connect } from "react-redux";
const mapStateToProps = state => {
  console.log(state);
  return { articles: state.articles };
};

// function Articles(props) {
//   console.log(props);

//   return (
//     <div>
//       <ul>
//         {props.articles.map(article => (
//           <li>{article.title}</li>
//         ))}
//       </ul>
//     </div>
//   );
// }

const ConnectedList = ({ articles }) => {
  console.log(articles);
  return (
    <ul>
      {articles.map(article => (
        <li>{article.title}</li>
      ))}
    </ul>
  );
};

const List = connect(mapStateToProps)(ConnectedList);
export default List;
