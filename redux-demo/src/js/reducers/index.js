import { ADD_ARTICLE } from "../constants/action-types";

const initialState = { articles: [{ title: "first article" }] };

function rootReducer(state = initialState, action) {
  // switch (action.type) {
  //   case ADD_ARTICLE:
  //     console.log(action);
  //     const newState = { ...state,  };
  //     // newState.articles.push(action.payload);
  //     // console.log(newState);
  //     return newState;
  //     // state = { ...newState };
  //     // return state;
  //     break;

  //   default:
  //     break;
  // }
  // return state;
  if (action.type === ADD_ARTICLE) {
    return Object.assign({}, state, {
      articles: state.articles.concat(action.payload)
    });
  }
  return state;
}

export default rootReducer;
