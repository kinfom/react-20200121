import store from "./store/index";
import { addArticle } from "./actions/index";

window.mystore = store;
window.addArticle = addArticle;
