import React, { Component } from "react";
import { connect } from "react-redux";
import { addArticle } from "./js/actions/index";

function mapDispatchToProps(dispatch) {
  return { addArticle: article => dispatch(addArticle(article)) };
}

class ConnectedForm extends Component {
  state = {};
  inputref = React.createRef();
  formHandler = e => {
    e.preventDefault();
    const title = this.inputref.current.value;
    this.props.addArticle({ title: title });
  };
  render() {
    console.log(this.props);

    return (
      <div>
        <h3>Add Article</h3>
        <form onSubmit={this.formHandler}>
          <label htmlFor="">Title</label>
          <input ref={this.inputref} type="text" />
          <button type="submit">Save</button>
        </form>
      </div>
    );
  }
}

const Form = connect(null, mapDispatchToProps)(ConnectedForm);
export default Form;
